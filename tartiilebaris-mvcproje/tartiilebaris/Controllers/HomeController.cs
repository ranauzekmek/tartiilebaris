﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Controllers
{
    public class HomeController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();

        public ActionResult Index()
        {
            var haber = db.haberler.ToList();
            return View(haber);
        }

        [ChildActionOnly]
        public ActionResult _Slider()
        {
            var liste = db.slider.Where(x => x.baslangıc < DateTime.Now && x.bitis > DateTime.Now).OrderByDescending(x => x.ID);
            return View(liste);
        }
       [Authorize(Roles = "kullanıcı")]
        public ActionResult Contact()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult contact(iletisim model)
        {
                if (ModelState.IsValid)
                {
                
                    db.iletisim.Add(model);
                }
            return RedirectToAction("Index");
            }
        public ActionResult Sagliklibeslenme()
        {
            var res = db.saglilibeslenme.ToList();
            return View(res);
        }
        public ActionResult Diyetcesitleri()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Diyetyemekleri()
        {
            var liste = db.yemek.ToList();
            return View(liste);
        }
        public ActionResult Egzersizler()
        {
            var liste = db.egzersiz.ToList();
            return View(liste);
        }
        public ActionResult Yapimasamasi()
        {
            ViewBag.Message = "Yapım aşamasındadır!!!";

            return View();
        }
        public ActionResult Eyndy()
        {
            var liste = db.habersayfa.Where(x => x.ID == 1).ToList();
            return View(liste);
        }
        public ActionResult Dogalzayiflama()
        {
            var liste = db.habersayfa.Where(x => x.ID >= 12 && x.ID <= 20).ToList();
            return View(liste);
        }
        public ActionResult Detoks()
        {
            var liste = db.habersayfa.Where(x => x.ID >= 21 && x.ID <= 25).ToList();
            return View(liste);
        }
        public ActionResult Cay()
        {
            var liste = db.habersayfa.Where(x => x.ID >= 2 && x.ID <= 6).ToList();
            return View(liste);
        }
        public ActionResult Ihesaplar()
        {
            ViewBag.Message = "Yapım aşamasındadır!!!";

            return View();
        }

        public ActionResult VKE()
        {
            ViewBag.Message = "Yapım aşamasındadır!!!";

            return View();
        }
        public ActionResult GKI()
        {
            var liste = db.habersayfa.Where(x => x.ID == 26).ToList();
            return View(liste);
        }
        public ActionResult FB()
        {
            ViewBag.Message = "Yapım aşamasındadır!!!";

            return View();
        }
        public ActionResult Mhb()
        {
            var liste = db.habersayfa.Where(x => x.ID >= 7 && x.ID <= 11).ToList();
            return View(liste);
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

    }
}