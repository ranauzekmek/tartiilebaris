﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class HabersayfaController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Habersayfa
        [Authorize(Roles = "editor")]
        public ActionResult Index()
        {
            var liste = db.habersayfa.ToList();
            return View(liste);
        }

        public ActionResult habersayfaAdd()
        {
            return View();
        }
        public ActionResult addhabersayfa(habersayfa model)
        {
            if (ModelState.IsValid)
            {
                db.habersayfa.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult habersayfaedit(int id)
        {
            var model = db.habersayfa.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult habersayfaedit(habersayfa model)
        {
            if (ModelState.IsValid)
            {
                db.habersayfa.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult habersayfadelete(int id)
        {
            var model = db.habersayfa.Find(id);
            db.habersayfa.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}


    }
}