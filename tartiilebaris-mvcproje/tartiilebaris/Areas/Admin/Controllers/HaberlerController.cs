﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class HaberlerController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Haberler
        [Authorize(Roles = "editor")]
        public ActionResult Index()
        {
            var liste = db.haberler.ToList();
            return View(liste);
        }
        public ActionResult HaberlerAdd()
        {
            return View();
        }
        public ActionResult addhaberler(haberler model)
        {
            if (ModelState.IsValid)
            {
                db.haberler.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult haberleredit(int id)
        {
            var model = db.haberler.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult haberleredit(haberler model)
        {
            if (ModelState.IsValid)
            {
                db.haberler.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult haberlerdelete(int id)
        {
            var model = db.haberler.Find(id);
            db.haberler.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

    }
}