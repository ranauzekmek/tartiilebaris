﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {

        tartiilebarisEntities db = new tartiilebarisEntities();
        [Authorize(Roles = "editor")]
        // GET: Admin/Slider
        public ActionResult Index()
        {
            var model = db.slider.OrderByDescending(x => x.ID).ToList();
            return View(model);
        }
        public ActionResult add()
        {

            return View();
        }
        public ActionResult addslider(slider model)
        {
            if (ModelState.IsValid)
            {
                db.slider.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult edit(int id)
        {
            var model = db.slider.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult edit(slider model)
        {
            if (ModelState.IsValid)
            {
                db.slider.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult delete(int id)
        {
            var model = db.slider.Find(id);
            return View(model);
        }

        public ActionResult deleteconfirm(int id)
        {
            var model = db.slider.Find(id);
            db.slider.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}