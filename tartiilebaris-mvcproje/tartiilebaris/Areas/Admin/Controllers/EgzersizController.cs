﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class EgzersizController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Egzersiz
        [Authorize(Roles = "editor")]
        public ActionResult Index()
        {
            var liste = db.egzersiz.ToList();
            return View(liste);
        }
        public ActionResult EgzersizAdd()
        {
            return View();
        }
        public ActionResult addegzersiz(egzersiz model)
        {
            if (ModelState.IsValid)
            {
                db.egzersiz.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult egzersizedit(int id)
        {
            var model = db.egzersiz.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult egzersizedit(egzersiz model)
        {
            if (ModelState.IsValid)
            {
                db.egzersiz.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult egzersizdelete(int id)
        {
            var model = db.egzersiz.Find(id);
            db.egzersiz.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

    }
}