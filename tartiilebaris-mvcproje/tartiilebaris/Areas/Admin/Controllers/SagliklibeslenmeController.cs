﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class SagliklibeslenmeController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Sagliklibeslenme
        [Authorize(Roles = "editor")]
        public ActionResult Index()
        {
            var liste = db.saglilibeslenme.ToList();
            return View(liste);
        }
        public ActionResult sagliklibeslenmeAdd()
        {
            return View();
        }
        public ActionResult addsagliklibeslenme(saglilibeslenme model)
        {
            if (ModelState.IsValid)
            {
                db.saglilibeslenme.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult sagliklibeslenmeedit(int id)
        {
            var model = db.saglilibeslenme.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult sagliklibeslenmeedit(saglilibeslenme model)
        {
            if (ModelState.IsValid)
            {
                db.saglilibeslenme.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult sagliklibeslenmedelete(int id)
        {
            var model = db.saglilibeslenme.Find(id);
            db.saglilibeslenme.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}