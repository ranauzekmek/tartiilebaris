﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class IletisimController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Egzersiz
        [Authorize(Roles = "editor")]
        public ActionResult Index()
        {
            var liste = db.iletisim.ToList();
            return View(liste);
        }
        public ActionResult iletisimAdd()
        {
            return View();
        }
        public ActionResult addiletisim(iletisim model)
        {
            if (ModelState.IsValid)
            {
                db.iletisim.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult iletisimedit(int id)
        {
            var model = db.iletisim.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult iletisimedit(iletisim model)
        {
            if (ModelState.IsValid)
            {
                db.iletisim.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult iletisimdelete(int id)
        {
            var model = db.iletisim.Find(id);
            db.iletisim.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

    }
}