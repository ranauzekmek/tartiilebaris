﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tartiilebaris.Areas.Admin.Controllers
{
    [Authorize(Roles ="editor")]
    public class YemekController : Controller
    {
        tartiilebarisEntities db = new tartiilebarisEntities();
        // GET: Admin/Yemek
        public ActionResult Index()
        {
            var liste = db.yemek.ToList();
            return View(liste);
        }
        public ActionResult yemekAdd()
        {
            return View();
        }
        public ActionResult addyemek(yemek model)
        {
            if (ModelState.IsValid)
            {
                db.yemek.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult yemekedit(int id)
        {
            var model = db.yemek.Find(id);
            //db.slider.FirstOrDefault(x => x.ID == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult yemekedit(yemek model)
        {
            if (ModelState.IsValid)
            {
                db.yemek.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public ActionResult yemekdelete(int id)
        {
            var model = db.yemek.Find(id);
            db.yemek.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult deletehaberler(int id)
        //{
        //    var model = db.haberler.Find(id);
        //    db.haberler.Remove(model);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}