﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tartiilebaris.Areas.Admin.Models;
using tartiilebaris.Models;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class RolYonetimiController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetimi
        [Authorize(Roles = "yonetici")]
        public ActionResult Index()
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolmanager = new RoleManager<IdentityRole>(rolestore);

            var model = rolmanager.Roles.ToList();
            return View(model);
        }
        public ActionResult rolekle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult rolekle(roleklemodel rol)
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolmanager = new RoleManager<IdentityRole>(rolestore);

            if (rolmanager.RoleExists(rol.rolad)==false)
            {
                rolmanager.Create(new IdentityRole(rol.rolad));
            }
            return RedirectToAction("Index");

        }
        public ActionResult rolkullaniciekle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult rolkullaniciekle(rolkullanicieklemodel model)
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolmanager = new RoleManager<IdentityRole>(rolestore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.kullaniciad);

            if (!userManager.IsInRole(kullanici.Id, model.rolad))
            {
                userManager.AddToRole(kullanici.Id, model.rolad);
            }
            return RedirectToAction("Index");
        }
    }
}