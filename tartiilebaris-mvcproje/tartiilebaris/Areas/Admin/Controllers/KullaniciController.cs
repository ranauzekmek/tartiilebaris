﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tartiilebaris.Models;

namespace tartiilebaris.Areas.Admin.Controllers
{
    public class KullaniciController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/Kullanici
        [Authorize(Roles = "yonetici")]
        public ActionResult Index()
        {
            var rolestore = new RoleStore<IdentityRole>(context);
            var rolmanager = new RoleManager<IdentityRole>(rolestore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var model = userManager.Users.ToList();
            return View(model);
        }
        public ActionResult kullanicidelete(int id)
        {
            var model =context.Find(id);
            context.Remove(model);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
   
}