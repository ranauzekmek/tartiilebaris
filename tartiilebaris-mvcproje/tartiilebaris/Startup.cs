﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tartiilebaris.Startup))]
namespace tartiilebaris
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
